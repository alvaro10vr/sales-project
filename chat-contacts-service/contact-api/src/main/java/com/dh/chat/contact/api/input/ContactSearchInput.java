package com.dh.chat.contact.api.input;

/**
 * @author Santiago Mamani
 */
public class ContactSearchInput {

    private Boolean withoutAccountOwner;

    private String information;

    public Boolean getWithoutAccountOwner() {
        return withoutAccountOwner;
    }

    public void setWithoutAccountOwner(Boolean withoutAccountOwner) {
        this.withoutAccountOwner = withoutAccountOwner;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }
}

package com.dh.chat.contact.service.controller.api;

import com.dh.chat.contact.api.input.ContactCreateInput;
import com.dh.chat.contact.api.model.Contact;
import com.dh.chat.contact.service.command.ContactCreateCmd;
import com.dh.chat.contact.service.controller.Constants;
import com.dh.chat.contact.service.model.impl.ContactBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = Constants.ContactTag.NAME,
        description = Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SECURE_CONTACTS)
@RequestScope
public class ContactCreateController {

    @Autowired
    private ContactCreateCmd contactCreateCmd;

    @ApiOperation(
            value = "Create a contact single"
    )
    @RequestMapping(method = RequestMethod.POST)
    public Contact createContact(@RequestHeader("Account-ID") Long accountId,
                                 @RequestHeader("User-ID") Long userId,
                                 @RequestBody ContactCreateInput input) {
        contactCreateCmd.setAccountId(accountId);
        contactCreateCmd.setInput(input);
        contactCreateCmd.execute();

        return ContactBuilder.getInstance(contactCreateCmd.getContact()).build();
    }
}

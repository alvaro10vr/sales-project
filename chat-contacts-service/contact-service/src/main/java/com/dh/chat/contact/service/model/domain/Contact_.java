package com.dh.chat.contact.service.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Santiago Mamani
 */
@StaticMetamodel(Contact.class)
public class Contact_ {
    public static volatile SingularAttribute<Contact, Long> id;

    public static volatile SingularAttribute<Contact, Long> userId;

    public static volatile SingularAttribute<Contact, Long> accountId;

    public static volatile SingularAttribute<Contact, String> email;

    public static volatile SingularAttribute<Contact, String> name;

    public static volatile SingularAttribute<Contact, String> avatarId;

    public static volatile SingularAttribute<Contact, Date> createdDate;

    public static volatile SingularAttribute<Contact, Detail> detail;
}

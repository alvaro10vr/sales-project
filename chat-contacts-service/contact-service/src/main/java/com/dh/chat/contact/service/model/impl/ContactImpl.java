package com.dh.chat.contact.service.model.impl;

import com.dh.chat.contact.api.model.Contact;
import lombok.Data;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
@Data
public class ContactImpl implements Contact {

    private Long contactId;

    private Long userId;

    private Long accountId;

    private String email;

    private String name;

    private String avatarId;

    private Date createdDate;
}

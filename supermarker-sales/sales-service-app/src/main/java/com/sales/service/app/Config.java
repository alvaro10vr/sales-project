package com.sales.service.app;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alvaro Veliz
 */
@Configuration
@ComponentScan("com.sales.service.app")
@EnableAutoConfiguration
@EnableFeignClients
public class Config {

}

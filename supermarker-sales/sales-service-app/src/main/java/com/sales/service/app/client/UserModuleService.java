package com.sales.service.app.client;

import com.sales.service.app.input.SalesContactInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alvaro Veliz
 */
@Service
public class UserModuleService {

    @Autowired
    private UserModuleClient client;

    public SalesContactInput createSalesContact(SalesContactInput input) {
        return client.createSalesContact(input);
    }
}

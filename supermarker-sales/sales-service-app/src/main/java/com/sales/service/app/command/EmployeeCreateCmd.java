package com.sales.service.app.command;

import com.jatun.open.tools.blcmd.annotations.AsynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.sales.service.app.input.EmployeeCreateInput;
import com.sales.service.app.model.domain.Employee;
import com.sales.service.app.model.repository.EmployeeRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

/**
 * @author Alvaro Veliz
 */
@AsynchronousExecution
public class EmployeeCreateCmd implements BusinessLogicCommand {

    @Setter
    private EmployeeCreateInput input;

    @Getter
    private Employee employee;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Getter
    @Value("${position.dev}")
    private String position;


    @Override
    public void execute() {

        Employee employeeInstance = composeEmployeeInstance();
        employee = employeeRepository.save(employeeInstance);
    }

    private Employee composeEmployeeInstance() {
        Employee instance = new Employee();
        instance.setPosition("QA");
        instance.setEmail(input.getEmail());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setGender(input.getGender());

        instance.setIsDeleted(Boolean.FALSE);
        instance.setCreatedDate(new Date());


        if (null != input.getPosition() && getPosition().toLowerCase().contains(input.getPosition().toLowerCase())) {
            instance.setPosition("DEV");
        }

        return instance;
    }
}

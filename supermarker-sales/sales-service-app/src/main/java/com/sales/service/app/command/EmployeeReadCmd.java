package com.sales.service.app.command;

import com.jatun.open.tools.blcmd.annotations.AsynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.sales.service.app.model.domain.Employee;
import com.sales.service.app.model.repository.EmployeeRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Alvaro Veliz
 */
@AsynchronousExecution
public class EmployeeReadCmd implements BusinessLogicCommand {

    @Setter
    private String email;

    @Getter
    private Employee employee;

    @Autowired
    private EmployeeRepository employeeRepository;


    @Override
    public void execute() {
        //employee = employeeRepository.findByEmail(email).orElse(new Employee());
        employee = employeeRepository.findEmployeeByEmail(email).orElse(new Employee());
    }

}

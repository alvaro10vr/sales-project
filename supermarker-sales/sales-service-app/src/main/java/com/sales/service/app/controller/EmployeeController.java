package com.sales.service.app.controller;

import com.sales.service.app.client.UserModuleService;
import com.sales.service.app.command.EmployeeCreateCmd;
import com.sales.service.app.command.EmployeeReadCmd;
import com.sales.service.app.input.EmployeeCreateInput;
import com.sales.service.app.input.SalesContactInput;
import com.sales.service.app.model.domain.Employee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Alvaro Veliz
 */
@Api(
        tags = "Employee rest AVR",
        description = "Operations over employee"
)
@RestController
@RequestMapping("/public/employees")
@RequestScope
public class EmployeeController {

    @Autowired
    private EmployeeCreateCmd employeeCreateCmd;

    @Autowired
    private EmployeeReadCmd employeeReadCmd;

    @Autowired
    private UserModuleService userModuleService;

    @ApiOperation(
            value = "endpoint to create employee"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create employee"
            )
    })
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput employeeCreateInput) {
        employeeCreateCmd.setInput(employeeCreateInput);
        employeeCreateCmd.execute();
        return employeeCreateCmd.getEmployee();

    }

    @ApiOperation(
            value = "get employee"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create employee"
            )
    })
    @RequestMapping(method = RequestMethod.GET, value = "readEmployee")
    public Employee getEmployee(@RequestParam("email") String email) {
        employeeReadCmd.setEmail(email);
        employeeReadCmd.execute();
        return employeeReadCmd.getEmployee();

    }

    @ApiOperation(
            value = "create sales contact"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create sales contact"
            )
    })
    @RequestMapping(method = RequestMethod.POST, value = "createSalesContact")
    public SalesContactInput createSalesContactInput(@RequestBody SalesContactInput input) {

        return userModuleService.createSalesContact(input);
    }

}

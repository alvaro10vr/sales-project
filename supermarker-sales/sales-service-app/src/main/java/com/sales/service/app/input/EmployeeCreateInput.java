package com.sales.service.app.input;

import com.sales.service.app.model.domain.PersonGender;
import lombok.Data;

/**
 * @author Alvaro Veliz
 */
@Data
public class EmployeeCreateInput {

    private String position;
    private String email;
    private String firstName;
    private String lastName;
    private PersonGender gender;

}

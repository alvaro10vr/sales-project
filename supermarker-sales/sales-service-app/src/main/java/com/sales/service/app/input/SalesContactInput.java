package com.sales.service.app.input;

import java.util.Date;

/**
 * @author Alvaro Veliz
 */

public class SalesContactInput {

    private String email;

    private String name;

    private Date date;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

package com.sales.service.app.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Alvaro Veliz
 */
@Entity
@Table(name = "client")
@Data
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "clientid", referencedColumnName = "personid")
})
public class Client extends Person {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastPurchase", nullable = false, updatable = false)
    private Date lastPurchase;

}

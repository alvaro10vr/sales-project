package com.sales.service.app.model.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * @author Alvaro Veliz
 */
@Entity
@Table(name = "employee")
@Data
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid", referencedColumnName = "personid")
})
public class Employee extends Person {

    @Column(name = "position")
    private String position;
}

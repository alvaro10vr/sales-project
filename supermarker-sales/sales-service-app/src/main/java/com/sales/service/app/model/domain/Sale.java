package com.sales.service.app.model.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Alvaro Veliz
 */
@Entity
@Table(name = "sale")
public class Sale {

    @Id
    @Column(name = "saleid", nullable = false, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "numbersale")
    private Long numberSale;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createddate", nullable = false, updatable = false)
    private Date createdDate;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saleemployeeid", referencedColumnName = "employeeid", nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saleclientid", referencedColumnName = "clientid", nullable = false)
    private Client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
